<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:t="http://www.example.org/documenttests" >
<xsl:output method="xml"/>

    <xsl:param name="secondreport" />


    <xsl:template match="/t:documenttestsreport">
        <xsl:copy>
             <xsl:apply-templates select="@*|node()" />
<!--
            <xsl:copy-of select="document('ApplicationData.xml')" />
            <xsl:copy-of select="document('MetricList.xml')" />
-->	    
        </xsl:copy>
    </xsl:template>

    <xsl:template match="t:input">
       <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="t:target">
       <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="t:testreport">
        <xsl:variable name="report">
            <xsl:value-of select="@name" />
        </xsl:variable>
        <xsl:copy>
	     <xsl:copy-of select="@*"/><!--copy of all attributes-->
             <xsl:apply-templates select="t:input" />
             <xsl:apply-templates select="t:target" />
	     <xsl:copy-of select="document($secondreport)//t:documenttestsreport/t:testreport[@name=$report]/t:target" />	     


<!--
	     <xsl:copy-of select="document('/tmp/report/report.xml')//t:documenttestsreport/t:testreport[@name=$report]/t:target" />	     
	     <xsl:copy-of select="document('/tmp/report/report.xml')///t:target" />
-->

        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
