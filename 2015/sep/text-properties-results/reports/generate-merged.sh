#!/bin/bash

rm -f /tmp/report.xml
cp word-and-lo4/report.xml  /tmp/report.xml
/Develop/officeshots/odfautoresults/report-merge.sh ./google/report.xml /tmp/report.xml >| /tmp/reportB.xml
mv -f /tmp/reportB.xml /tmp/report.xml
/Develop/officeshots/odfautoresults/report-merge.sh ./local-abiword-lo5-webodf/report.xml /tmp/report.xml >| /tmp/reportB.xml
mv -f /tmp/reportB.xml /tmp/report.xml

cp -avf /tmp/report.xml .
#xmlstarlet tr /Develop/officeshots/odfautotests/odfautotests/report2html.xsl report.xml > report.html
xmlstarlet tr /Develop/officeshots/odfautotests/odfautotests/report2html.xsl -s expanded=1 report.xml >| report.html

if=report.html
sed -i 's.\\./.g' "$if"
sed -i 's|href="[^/]*/output/|href="output/|g' "$if"
sed -i 's|src="[^/]*/output/|src="output/|g' "$if"
