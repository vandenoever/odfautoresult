#!/bin/bash

echo "This script will expand the odt files in the given paths to "
echo " allow easy inspection of the files contained in the zip structure"
echo " and also create an xmllint formatted version of content.xml "
echo " to allow easy human consideration and reasoning."

for if in "$*"; do
    echo "expanding: $if"
    for odtpath in $(find $if -name "*.odt"); do
	mkdir -p "$odtpath.expanded"
	unzip -o "$odtpath" -d "$odtpath.expanded"
	xmllint --format "$odtpath.expanded/content.xml" >| "$odtpath.expanded/content-formatted.xml"
#	xmllint --format "$odtpath.expanded/styles.xml " >| "$odtpath.expanded/styles-formatted.xml"
    done
done
