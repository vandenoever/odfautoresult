
report.html: *odt output/word/*odt output/libreoffice4/*odt
	java -jar ~/javabin/odftester.jar \
		-i `pwd` -r ./output \
		-t $(TESTFILE)
